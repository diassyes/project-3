package main

import (
	"fmt"
	"gitlab.com/diassyes/dodos_module_3"
)

func main() {
	fmt.Println(dodos_module_3.Concat("Dias", "Yeszhanov"))
	fmt.Println(dodos_module_3.GetSolveQuadraticEquation(1, -2, -3))
	fmt.Println(dodos_module_3.ShiftBits(2, false, 2))
}